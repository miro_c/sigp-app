
import axios from 'axios';
// import {UserFinal, Shelter} from '../store/UserReducer';
import {BASE_URL, REQUEST_PARAM_API_KEY, API_KEY} from '../api/constants';

const axiosInstance = axios.create({
	baseURL: BASE_URL,
	timeout: 15000,
});

export const MovieHttpApi = {
    getMovieList: function(movieTitle: string) {
        return axiosInstance.request({
            method: "GET",
            url: `/${REQUEST_PARAM_API_KEY}${API_KEY}&s=${movieTitle}` // example http call -> http://omdbapi.com/?apikey=[YOUR-API-KEY]&s=Batman
        });
    },

    getMovieDetail: function(movieId: string) {
        return axiosInstance.request({
            method: "GET",
            url: `/${REQUEST_PARAM_API_KEY}${API_KEY}&i=${movieId}` // example http call -> http://omdbapi.com/?apikey=[YOUR-API-KEY]&i=tt0372784
        });
    }
}




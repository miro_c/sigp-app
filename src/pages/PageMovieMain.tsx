import { FC } from 'react';
import { Link } from "react-router-dom";
import camera_find from '../assets/images/camera_find.svg';
import camera_star from '../assets/images/camera_star.svg';

const PageMovieMain: FC = () => {

    return (
        <>
            <p className="welcome_title">Welcome to SIGP Movie finder</p>
       
            <nav className="navigation_main">
                <ul className="navigation_ul">

                    {/* Link search */}
                    <Link to="/search" className="link_wrapper">
                        <img className="menu_icon" src={camera_find} alt="Logo" />
                        <li className="menu_icon_text">Find Movie</li>
                    </Link>

                    {/* Link Favorites */}
                    <Link to="/favorites" className="link_wrapper">
                        <img className="menu_icon" src={camera_star} alt="Logo" />
                        <li className="menu_icon_text">Favorite Movies</li>
                    </Link>
                </ul>
            </nav>
        </>
    );
}

export default PageMovieMain;
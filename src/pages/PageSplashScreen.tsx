import { useHistory } from 'react-router-dom';
import { useEffect } from 'react';
import camera from '../assets/images/camera.svg';

const PageSplashScreen = () => {
  const history = useHistory();

  useEffect(() => {
    goToNextPage()

  });

  return (
    <>
      <div className="main_wrapper">
        <div className="splash_main">
          <img src={camera} alt="Logo" />
        </div>

      </div>
    </>
  );

  function goToNextPage() {
    setTimeout(() => {
      history.replace({
        pathname: '/main'
      });
    }, 2000);
  }
}

export default PageSplashScreen;
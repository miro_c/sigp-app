import { FC, useState } from 'react';
import { Link } from "react-router-dom";
import { Movie } from '../store/MovieReducer';
import MovieList  from '../components/MovieList';
import MovieSearch  from '../components/MovieSearch';


const PageMovieSearch: FC = () => {
  const [movieList, setMovieList] = useState<Movie[]>([]);

  return (
    <>
      {/* Movie Search */}
      <MovieSearch setMovieListAction={setMovieList}/>

      {/* Movie list main */}
      {movieList.length > 0 ? <MovieList movieList={movieList}></MovieList> : <div className="empty_list_wrapper"><p className="empty_list_text">Empty movie list...</p></div>}

      <Link to="/main">&#8617; Back home</Link>
    </>
  );

}

export default PageMovieSearch;
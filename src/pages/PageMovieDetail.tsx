import { Link, useLocation } from "react-router-dom";
import { FC, useRef, useEffect, useState } from 'react';
import DispatcherManager from '../store/DispatcherManager';
import { AppState } from '../store/AppState';
import { Movie, Rating, MovieAction } from '../store/MovieReducer';
import { useDispatch, useSelector } from 'react-redux';
import star_default from '../assets/images/star_disable.svg';
import star_active from '../assets/images/star_active.svg';
import Snackbar from '@material-ui/core/Snackbar';
import { useHistory } from 'react-router-dom';


interface MovieProps {
  movieDetail: Movie | null
}

const PageMovieDetail: FC<MovieProps> = (props: MovieProps) => {
  const [starIsChecked, setStarChecked] = useState(Boolean);
  const [open, setOpen] = useState(false);
  const [transition, setTransition] = useState(undefined);
  const history = useHistory();
  const dispatch = useDispatch();
  const location = useLocation();

  if (!props.movieDetail) {
    return (
      <>
        <div className="text_loading">Loading movie detail...</div>
      </>
    );
  }

  const addFavoriteAction = (favoriteMovie: Movie | null) => {
    setStarChecked(true);
    handleClick()

    // --- add movie to favorite movie ---
    DispatcherManager.getInstance().dispatchFavoriteMovie(dispatch, favoriteMovie);
  }

  const handleClick = () => {
    setOpen(true);

    setTimeout(() => {
      handleClose()
    }, 2000);
  };

  const handleClose = () => {
    setOpen(false);

    setTimeout(() => {
      history.replace({
        pathname: '/search'
      });
    }, 500);

  };

  return (
    <>
      <img className="movie_poster_detail" src={props.movieDetail?.Poster} alt="Movie Poster" />

      <div className="movie_title_wrapper">
        <p className="movie_info_span_header">{props.movieDetail?.Title}</p>
        <img className="rating_star" onClick={() => addFavoriteAction(props.movieDetail)} src={starIsChecked ? star_active : star_default} alt="detail movie" />
        {!starIsChecked && <p className="movie_info_text_extra_small">add to favorites</p>}
      </div>

      <p className="movie_info_text">{props.movieDetail?.Country}</p>
      <p className="movie_info_text">{props.movieDetail?.Awards}</p>
      <p className="movie_info_text_small">({props.movieDetail?.Actors})</p>

      <Link to="/search">&#8617; Back to search</Link>

      <Snackbar
        open={open}
        onClose={handleClose}
        TransitionComponent={transition}
        message="Movie added to favorites"
      />
    </>
  );
}

export default PageMovieDetail;
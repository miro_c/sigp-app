
export const MOVIE_DETAIL = "MOVIE_DETAIL";
export const MOVIE_ADD_TO_FAVORITE = "MOVIE_ADD_TO_FAVORITE";

export interface Movie {
    Actors: string;
    Awards: string;
    BoxOffice: string;
    Country: string;
    DVD: string;
    Director: string;
    Genre: string;
    Language: string;
    Metascore: string;
    Plot: string;
    Poster: string;
    Production: string;
    Rated: string;
    Ratings: Array<Rating>;
    Released: string;
    Response: string;
    Runtime: string;
    Title: string;
    Type: string;
    Website: string;
    Writer: string;
    Year: string;
    imdbID: string;
    imdbRating: string;
    imdbVotes: string;
}

export interface Rating {
    Source: string;
    Value: string;
}

export interface MovieAction {
    type: string;
    payload: Movie | null;
}

export const MovieReducer = (state: Movie | null = null, action: MovieAction): Movie | null => {
    switch (action.type) {
        case MOVIE_DETAIL:
            return action.payload;

        case MOVIE_ADD_TO_FAVORITE:
            return action.payload;

        default:
            return state;
    }
};


export const MOVIE_ACTION_TYPE = "MOVIE_ACTION_TYPE";

export enum ActionType {
    ACTION_ADD = "ADD", ACTION_DELETE = "DELETE"
}

export interface MovieActionType {
    actionFired: boolean;
    action_type: ActionType;
    page_step: number;
}

export interface MovieAction {
    type: string;
    payload: MovieActionType | null;
}

export const MovieActionReducer = ( state: MovieActionType | null = null, action: MovieAction): MovieActionType | null => {
    switch(action.type) {
        case MOVIE_ACTION_TYPE:
            return action.payload;
        default:
            return state;
    }
};
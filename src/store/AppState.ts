import { combineReducers } from "redux";
import { MovieReducer } from "./MovieReducer";
import { MovieActionReducer } from "./MovieActionReducer";

export const rootReducer = combineReducers({
    movie: MovieReducer,
    movie_action: MovieActionReducer
});

export type AppState = ReturnType<typeof rootReducer>;

import { MOVIE_DETAIL, MOVIE_ADD_TO_FAVORITE,  Movie} from './MovieReducer';
import { MOVIE_ACTION_TYPE} from './MovieActionReducer';
import { Dispatch } from "react";


export default class DispatcherManager {
    private static instance: DispatcherManager;

    private constructor() { }

    public static getInstance(): DispatcherManager {
        if (!DispatcherManager.instance) {
            DispatcherManager.instance = new DispatcherManager();
        }

        return DispatcherManager.instance;
    }

    /***********************************************************
     *        --- Dispatcher Actions ---
     * *********************************************************/
     dispatchInitState(dispatcher: Dispatch<any>): void {
        dispatcher({
            type: MOVIE_ACTION_TYPE,
            payload: {
                page_step: 0
            }
        });
    }

    dispatchDetailMovie(dispatcher: Dispatch<any>, movie: Movie | null): void {
        dispatcher({
            type: MOVIE_DETAIL,
            payload: {
                ...movie
            }
        });
    }

    dispatchFavoriteMovie(dispatcher: Dispatch<any>, movie: Movie | null): void {
        dispatcher({
            type: MOVIE_ADD_TO_FAVORITE,
            payload: {
                ...movie
            }
        });
    }
   
}


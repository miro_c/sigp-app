import { BrowserRouter, Link, Route, Switch, useHistory, useLocation } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import PageMovieSearch from "./pages/PageMovieSearch"
import PageMovieDetail from "./pages/PageMovieDetail"
import PageMovieFavorite from "./pages/PageMovieFavorite"
import PageMovieMain from "./pages/PageMovieMain"
import PageSplashScreen from "./pages/PageSplashScreen"
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from './store/AppState';
import './App.css';


const App = () => {
  const MovieDetail = useSelector((state: AppState) => state.movie);

  if (MovieDetail) {
    console.log('Detail from App ' + MovieDetail)
  }

  return (
    <div className="App">
      <CssBaseline />
      <Container maxWidth="sm">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={PageSplashScreen} />
            <Route path='/main'>
              <PageMovieMain />
            </Route>
            <Route path='/search'>
              <PageMovieSearch />
            </Route>
            <Route path='/detail'>
              <PageMovieDetail movieDetail={MovieDetail}/>
            </Route>
            <Route path='/favorites'>
              <PageMovieFavorite />
            </Route>
          </Switch>
        </BrowserRouter>
      </Container>
    </div>
  );
}

export default App;
